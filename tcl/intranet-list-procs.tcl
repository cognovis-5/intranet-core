
namespace eval ::intranet::list {;}
ad_proc ::intranet::list::create {
    {-name:required ""}
    {-multirow_name:required ""}
    {-view_name:required ""}
    {-key ""}
    {-bulk_actions ""}
    {-bulk_action_export_vars ""}
    {-bulk_action_method ""}
    {-no_data ""}
    {-extend_varsVar ""}
    {-extend_scriptVar ""}
    {-return_url ""}
} {
    @author ND
} {
    if { $extend_varsVar ne {} } {
        upvar $extend_varsVar extend_vars
    }
    if { $extend_scriptVar ne {} } {
        upvar $extend_scriptVar extend_script
    }

    set current_user_id [auth::get_user_id]
    set user_is_admin_p [im_is_user_site_wide_or_intranet_admin $current_user_id]

    set view_id [db_string get_view_id "select view_id from im_views where view_name=:view_name"]

    set column_sql "
    select	column_id,
            column_name,
            column_render_tcl,
            visible_for,
            (order_by_clause is not null) as order_by_clause_exists_p
    from	im_view_columns
    where	view_id = :view_id
    order by sort_order, column_id
    "

    set extend_vars [list]
    set elements [list]
    set orderby [list]
    db_foreach column_list_sql $column_sql {
        if {"" == $visible_for || [eval $visible_for]} {


            set admin_link "" 
            if {$user_is_admin_p} {
                set admin_link "<a href=[export_vars -base "/intranet/admin/views/new-column" {return_url column_id {form_mode edit}}] target=\"_blank\">[im_gif wrench]</a>"
            }

            regsub -all " " $column_name "_" col_txt
            if { [regexp {^[a-zA-Z0-9_]+$} $col_txt] } {
                set col_txt [lang::message::lookup "" intranet-workflow.$col_txt $column_name]
            }
            set column_label "${col_txt}${admin_link}"

            set varname _$column_id
            lappend extend_vars $varname
            lappend elements $varname [list label $column_label display_template "@${multirow_name}.${varname};noquote@"]
            append extend_script "\n" "set $varname $column_render_tcl"

            if { $order_by_clause_exists_p } {
                lappend orderby $varname [list label $column_name]
            }
        }
    }


    set script [list \
        ::template::list::create \
            -name $name \
            -bulk_actions $bulk_actions \
            -bulk_action_export_vars $bulk_action_export_vars \
            -key $key \
            -no_data $no_data \
            -multirow $multirow_name \
            -elements $elements \
            -orderby $orderby]

    # execute in caller's context for bulk_action_export_vars to work
    uplevel $script

}

