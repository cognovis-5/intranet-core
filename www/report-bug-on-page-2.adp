<master src="../../intranet-core/www/master">
<property name="title">@title@</property>

<H1>#intranet-forum.Incident_Received#</H1>

<p>
#intranet-forum.Thank_you_for_submitting_your_incident#
</p>

	<table border=0 cellspacing=2 cellpadding=2 width="100%">
	<tr class=rowtitle>
	  <td class=rowtitle align=center colspan=2>
	    #intranet-forum.Incident_Information#
	  </td>
	</tr>
	<tr>
	  <td class="form-element">#intranet-forum.Your_Name#</td>
	  <td class="form-widget">@error_first_names@ @error_last_name@</td>
	</tr>
	<tr>
	  <td class="form-element">#intranet-forum.Your_Email#</td>
	  <td class="form-widget">@error_user_email@</td>
	</tr>
	<tr>
	  <td class="form-element">#intranet-forum.System_URL#</td>
	  <td class="form-widget">@system_url@</td>
	</tr>
	<tr>
	  <td class="form-element">#intranet-forum.Publisher_Name#</td>
	  <td class="form-widget">@publisher_name@</td>
	</tr>
	<tr>
	  <td class="form-element">#intranet-forum.Error_URL#</td>
	  <td class="form-widget">@error_url@</td>
	</tr>
	<tr>
	  <td class="form-element">#intranet-core.What_is_wrong#</td>
	  <td class="form-widget">@error_message@</td>
	</tr>
	<tr>
		<td class="form-element">#intranet-core.How_it_should_be_right#</td>
		<td class="form-widget">@error_info@</td>
	</tr>
	</table>


