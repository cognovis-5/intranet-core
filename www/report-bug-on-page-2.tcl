# /packages/intranet-forum/www/intranet/forum/new-system-error.tcl
#
# Copyright (C) 2003 - 2009 ]project-open[
#
# All rights reserved. Please check
# http://www.project-open.com/license/ for details.

ad_page_contract {
    Creates a new system error from a "Report this error" button.
    Works as an interface between the request procesor generating
    the incident and the forum module that works differntly then
    the old ACS ticket tracker.

    So there are several difficulties:
    - This page is publicly accessible, so it may be used to flood 
      the system with incidents

    @author frank.bergmann@project-open.com
} {
    { error_url:trim ""}
    { error_location:trim ""}
    { error_info:trim,allhtml ""}
    { error_message:trim,allhtml ""}
    { error_first_names:trim ""}
    { error_last_name:trim ""}
    { error_user_email:trim ""}
    { error_type:trim "standard"}
    { error_content:trim ""}
    { error_content_filename:trim ""}
    { core_version:trim ""}
    { package_versions:trim ""}
    { system_url:trim ""}
    { system_id:trim ""}
    { hardware_id:trim "" }
    { publisher_name ""}
    { platform "" } 
    { os ""} 
    { os_version ""}
}

# -----------------------------------------------------------------
# Defaults & Security
# -----------------------------------------------------------------

set current_user_id [auth::get_user_id]

ns_log Debug "new-system-incident: error_url=$error_url"
ns_log Debug "new-system-incident: error_info=$error_info"
ns_log Debug "new-system-incident: error_message=$error_message"
ns_log Debug "new-system-incident: error_first_names=$error_first_names"
ns_log Debug "new-system-incident: error_last_name=$error_last_name"
ns_log Debug "new-system-incident: error_user_email=$error_user_email"
ns_log Debug "new-system-incident: core_version=$core_version"
ns_log Debug "new-system-incident: package_versions=$package_versions"

# Maximum number of incidents per day per IP address
# Designed to avoid denial or service attacks
set max_dayily_incidents 3

set return_url "/intranet/"
set authority_id ""
set username ""

set title "New System Incident"

set system_owner_email [ad_parameter -package_id [im_package_forum_id] ReportThisErrorEmail]
set system_owner_id [db_string user_id "select min(user_id) from users where user_id > 0"]

# -----------------------------------------------------------------
# Get more debug information
# -----------------------------------------------------------------

set more_info "Generic Vars:\n"

# Extract variables from form and HTTP header
set header_vars [ns_conn headers]
set url [ns_conn url]

# UserId probably 0, except for returning users
set user_id [auth::get_user_id]
append more_info "user_id: $user_id\n"


set client_ip [ns_set get $header_vars "Client-ip"]
set referer_url [ns_set get $header_vars "Referer"]
set peer_ip [ns_conn peeraddr]
append more_info "client_ip: $client_ip\n"
append more_info "referer_url: $referer_url\n"
append more_info "peer_ip: $peer_ip\n"


append more_info "\nHeader Vars:\n"
foreach var [ad_ns_set_keys $header_vars] {
    set value [ns_set get $header_vars $var]
    append more_info "$var: $value\n"
}


# -----------------------------------------------------------------
# Lookup user_id or create entry
# Keep in mind that the email and other data might be completely fake.
# -----------------------------------------------------------------

ns_log Debug "Check if the user already has an account: $error_user_email"
set error_user_id [db_string user_id "select party_id from parties where lower(email) = lower(:error_user_email)" -default 0]

# Default if there was an error creating a new user
if {!$error_user_id} {
    # create user didn't succeed...
    set error_user_id $system_owner_id
}

# -----------------------------------------------------------------
# Find out the title line for the error
# -----------------------------------------------------------------

# Limit the shortened version of the url to 100 chars
# and put a space before every "&" to allow the line to break
set error_url_shortened [string range $error_url 0 100]
regsub -all {&} $error_url_shortened { &} error_url_shortened

# Determine the subject line. Do an effort to make it pretty.
set subject ""
if {[regexp {ERROR\:([^\n]*)} $error_info match error_descr]} {
    set subject "$error_url_shortened: $error_descr"
}

if {"" == $subject && [regexp {([^\n]*)} $error_info match error_descr]} {
    set subject "$error_url_shortened: $error_descr"
}

# Default - didn't find any reasonable piece of error code
if {"" == $subject} { set subject $error_url }

set message "
Error URL: $error_url
Error Location: $error_location
User Name: $error_first_names $error_last_name
User Email: $error_user_email

$more_info

Package Version(s): $core_version
Package Versions: $package_versions

Error Message:
$error_message

Error Info:
$error_info

"

acs_mail_lite::send -send_immediately -to_addr [ad_system_owner] -from_addr $error_user_email -subject $subject -body $message
