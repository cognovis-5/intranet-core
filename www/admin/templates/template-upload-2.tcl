# /packages/intranet-translation/www/upload-task-2.tcl
#
# Copyright (C) 2004 - 2009 ]project-open[
#
# All rights reserved (this is not GPLed software!).
# Please check http://www.project-open.com/ for licensing
# details.

ad_page_contract {
    insert a file into the file system
} {
    return_url
    upload_file
    cost_type_id
}

# ---------------------------------------------------------------------
# Defaults & Security
# ---------------------------------------------------------------------

set user_id [auth::require_login]
set template_path [im_filestorage_template_path]
set page_title "[_ intranet-translation.Upload_Successful]"
set context_bar [im_context_bar $page_title]


# -------------------------------------------------------------------
# Get the file
# -------------------------------------------------------------------

set max_n_bytes [im_parameter -package_key "intranet-filestorage" MaxNumberOfBytes "" 0]
set tmp_filename [ns_queryget upload_file.tmpfile]
im_security_alert_check_tmpnam -location "template-upload-2.tcl" -value $tmp_filename
set filesize [file size $tmp_filename]

if { $max_n_bytes && ($filesize > $max_n_bytes) } {
    set util_commify_number_max_n_bytes [util_commify_number $max_n_bytes]
    ad_return_complaint 1 "[_ intranet-translation.lt_Your_file_is_larger_t_1]"
    ad_script_abort
}

if {![regexp {^([a-zA-Z0-9_\-]+)\.([a-zA-Z_]+)\.([a-zA-Z]+)$} $upload_file match]} {
    ad_return_complaint 1 [lang::message::lookup "" intranet-core.Invalid_Template_format "
	<b>Invalid Template Format</b>:<br>
	Templates should have the format 'filebody.locale.ext'.<br>
        Example: 'invoice.en.odt' - An english OpenOffice ('.odt') template for invoices 
    "]
    ad_script_abort
}




# -------------------------------------------------------------------
# Copy the uploaded file into the template filestorage
# -------------------------------------------------------------------

if { [catch {
    ns_cp $tmp_filename "$template_path/$upload_file"
} err_msg] } {
    # Probably some permission errors
    ad_return_complaint 1 "[lang::message::lookup "" intranet-core.Error_Copying "Error Copying File"]:<br>
	<pre>$err_msg</pre>during command:
	<pre>ns_cp $tmp_filename $template_path/$upload_file</pre>
    "
    ad_script_abort
}

# -------------------------------------------------------------------
# Create a new category
# -------------------------------------------------------------------

set cat_exists_p [db_string ex "select count(*) from im_categories where category = :upload_file and category_type = 'Intranet Cost Template'"]
if {!$cat_exists_p} {

    set cat_id [db_nextval "im_categories_seq"]
    set cat_id_exists_p [db_string cat_ex "select count(*) from im_categories where category_id = :cat_id"]
    while {$cat_id_exists_p} {
	set cat_id [db_nextval "im_categories_seq"]
	set cat_id_exists_p [db_string cat_ex "select count(*) from im_categories where category_id = :cat_id"]
    }

    db_dml new_cat "
	insert into im_categories (
		category_id,
		category,
		category_type,
		enabled_p,
		aux_int1
	) values (
		nextval('im_categories_seq'),
		:upload_file,
		'Intranet Cost Template',
		't',
		:cost_type_id
	)
    "
}
set category_id [im_category_from_category -category $upload_file -category_type "Intranet Cost Template"]

# Check if we have a default dynfield for the cost centers
set short_name [db_string cost_type_short_name "
	select	short_name
	from	im_cost_types 
	where	cost_type_id = :cost_type_id
	"]

if {$short_name eq ""} {
	ad_returnerror "Missing configuration" "The short name for the cost type $cost_type_id is not configured in im_cost_types. This needs to be done in categories"
}

set column_name "default_${short_name}_template_id"


set cost_center_dynfields [im_dynfield::dynfields -object_type "im_cost_center"]
if {[lsearch $cost_center_dynfields "$column_name"]<0} {


	# Create the dynfield
	im_dynfield::attribute::add -object_type "im_cost_center" \
		-widget_name "cost_templates" \
		-attribute_name "$column_name" \
		-pretty_name "Template for [im_category_from_id $cost_type_id]" \
		-table_name "im_cost_centers" \
		-modify_sql_p "t"
		
	db_dml default_template "update im_cost_centers set $column_name = :category_id where parent_id = [im_cost_center_company] or cost_center_id = [im_cost_center_company]"
}

# (Re-) enable the category
db_dml enable "update im_categories set enabled_p = 't', aux_int1 = :cost_type_id where category_id = :category_id"


# Remove all permission related entries in the system cache
im_permission_flush

