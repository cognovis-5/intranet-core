# /packages/intranet-core/www/admin/templates/index.tcl
#
# Copyright (C) 2009 ]project-open[

ad_page_contract {
    Show the list of templates in the system
    @author frank.bergmann@project-open.com
} {
    { return_url "/intranet/admin/templates/index" }
    { enabled_p "t" }
    { cost_type_id ""}
}

# ------------------------------------------------------
# Defaults & Security
# ------------------------------------------------------

set user_id [auth::require_login]
set user_is_admin_p [im_is_user_site_wide_or_intranet_admin $user_id]
if {!$user_is_admin_p} {
    ad_return_complaint 1 "You have insufficient privileges to use this page"
    return
}

set page_title [lang::message::lookup "" intranet-core.Templates "Templates"]
set context_bar [im_context_bar $page_title]
set context ""
set find_cmd [parameter::get -package_id [im_package_core_id] -parameter "FindCmd" -default "/bin/find"]

set bgcolor(0) " class=rowodd"
set bgcolor(1) " class=roweven"


# ------------------------------------------------------
# Get the list of backup sets for restore
# ------------------------------------------------------

# Get the list of all backup sets under backup_path
set backup_path [im_backup_path]
set backup_path_exists_p [file exists $backup_path]
set not_backup_path_exists_p [expr {!$backup_path_exists_p}]
set enabled_values [list [list "Yes" "t"] [list "No" "f"]]
set cost_type_options [db_list_of_lists cost_type_options "select distinct im_name_from_id(aux_int1) as cost_type, aux_int1 from im_categories         where   category_type = 'Intranet Cost Template' order by cost_type"]
if {$cost_type_id eq ""} {unset cost_type_id}

template::list::create \
    -name templates \
    -key category_id \
    -elements {
		category {
		    label "Template Name"
		    link_url_col url
		}
		enabled_p {
		    label "Enabled?"
		}
		cost_type {
			label "#intranet-cost.Document_Type#"
			display_col "cost_type;noquote"
		}
    } \
    -bulk_actions { 
		"Enable Template" "template-enable" "Enable Template" 
		"Disable Template" "template-disable" "Disable Template" 
		"Delete Template" "template-delete" "Delete Template" 
    } \
    -bulk_action_method post \
    -bulk_action_export_vars { return_url } \
    -actions [list "Upload New Template" [export_vars -base template-upload] "Upload a new template"] \
    -filters {
	enabled_p {
	    label "Enabled?"
            values $enabled_values
	    where_clause {
                enabled_p = :enabled_p
	    }
	    has_default_p 1
	}
	cost_type_id {
	    label "Cost Type"
	    values $cost_type_options
	    where_clause {
		aux_int1 = :cost_type_id
	    }
	    has_default_p 0
	}
    }

db_multirow -extend { object_attributes_url url } templates templates_sql "
	select	category, enabled_p, im_name_from_id(aux_int1) as cost_type, category_id
	from	im_categories
	where	category_type = 'Intranet Cost Template'
    [template::list::filter_where_clauses -and -name "templates"]
	order by
		lower(category)

" {
    set object_attributes_url ""
    set url "/intranet/admin/templates/template-download?template_name=$category"
    set cost_type_url [export_vars -base "/intranet/admin/categories/one" -url {category_id}]
    if {$cost_type eq ""} {set cost_type "Add"}
    set cost_type "<center><a href='$cost_type_url'>$cost_type</a></center>"
}